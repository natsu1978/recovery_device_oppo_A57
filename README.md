TeamWin Recovery Project

Device configuration for OPPO A57  (_A57_)
=====================================================

Basic   | Spec Sheet
-------:|:-------------------------
CPU     | Octa-core 1.4 GHz Cortex-A53
CHIPSET | Qualcomm MSM8940 Snapdragon 435
GPU     | Adreno 505
Memory  | 3 GB
Shipped Android Version | Android 6.0.1 with ColorOS 3
Storage | 32 GB
MicroSD | Up to 256 GB (Hybrid)
Battery | 2900 mAh (non-removable)
Dimensions | 149.1 x 72.9 x 7.7 mm
Display | 720 x 1280 pixels, 5.2" IPS
Rear Camera  | 13.0 MP, LED flash
Front Camera | 16.0 MP
Release Date | November 2016

![OPPO A57](https://fdn2.gsmarena.com/vv/pics/oppo/oppo-a57-0.jpg "OPPO A57")
